package TP2018.Assignment4;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.junit.Test;

import Models.Account;
import Models.Bank;
import Models.Person;
import Models.SpendingAccount;

public class TestOps {
	   
	@Test
	   public void addPersonTest()
	   {
		   Bank bank = new Bank();
		   bank.addPerson("Florentina", "0744628331");
		   assertEquals(1, bank.getPersons().size());
	   }
	
	@Test
	   public void removePersonTest()
	   {
		   Bank bank = new Bank();
		   bank.addPerson("Florentina", "0744628331");
		   bank.removePerson(new Person("Florentina", "0744628331"));
		   assertEquals(0, bank.getPersons().size());
	   }
	
	@Test
	public void addAccountTest()	
	{
		 Bank bank = new Bank();
		 bank.addPerson("Fiorentina", "0744628231");
		 Account a = new SpendingAccount(0, 0);
		 
		 Map<Person, HashSet<Account>> bankk = bank.getBank();
		 
		 for(Person p : bankk.keySet())
			 if(p.getName().equals("Fiorentina") && p.getTelephoneNumber().equals("0744628231"))
				 {
				 	bank.addHolderAssociatedAccount(p, a);
				 	assertEquals(1, bank.getAccounts(p).size());
				 } 
	}

	@Test
	public void removeAccountTest()
	{
		 Bank bank = new Bank();
		 bank.addPerson("Fiorentina", "0744628231");
		 Account a = new SpendingAccount(0, 0);
		 
		 Map<Person, HashSet<Account>> bankk = bank.getBank();
		 
		 for(Person p : bankk.keySet())
			 if(p.getName().equals("Fiorentina") && p.getTelephoneNumber().equals("0744628231"))
				 {
				 	bank.addHolderAssociatedAccount(p, a);
				 	bank.removeHolderAssociatedAccount(p, a);
				 	assertEquals(0, bank.getAccounts(p).size());
				 } 
	}
	
	@Test
	public void editPersonTest()
	{
		 Bank bank = new Bank();
		 bank.addPerson("Fiorentina", "0744628231");
		 
		 Map<Person, HashSet<Account>> bankk = bank.getBank();
		 
		 for(Person p : bankk.keySet())
			 if(p.getName().equals("Fiorentina") && p.getTelephoneNumber().equals("0744628231"))
				 bank.editPerson(p, "Florentina", "0744628231");
		 
		 for(Person p : bank.getPersons())
		 {
			 if(p.getName().equals("Florentina"))
				 assertEquals("Florentina", p.getName());
		 }
	}
	
	@Test
	public void editPersonAccountTest()
	{
		//editHolderAssociatedAccount(Person holder, Account account, float newBalance)
		 Bank bank = new Bank();
		 bank.addPerson("Fiorentina", "0744628231");
		 Account a = new SpendingAccount(0, 0);
		 
		 Map<Person, HashSet<Account>> bankk = bank.getBank();
		 
		 for(Person p : bankk.keySet())
			 if(p.getName().equals("Fiorentina") && p.getTelephoneNumber().equals("0744628231"))
				 {
				 	bank.addHolderAssociatedAccount(p, a);
				 	bank.editHolderAssociatedAccount(p, a, 1000);
				 	for(Account ac : bank.getAccounts(p))
				 		if(ac.getBalance() == 1000)
				 			assertEquals(1, 1);
				 } 
	}
	
}
