package Presentation;


import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import Models.Bank;
import Models.Person;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ManagePersons extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTable table;
	private JButton btnNewButton = new JButton("Back");
	private Bank bank = new Bank();
	private JOptionPane frame = new JOptionPane();
	private Person selectedPerson = null;
	

	public ManagePersons() {
	
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		      
		    		backUpInfo();
		            System.exit(0);
		        
		    }
		});
		setBounds(100, 100, 657, 738);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAddNewPerson = new JLabel("Add New Person:");
		lblAddNewPerson.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAddNewPerson.setBounds(12, 35, 136, 16);
		contentPane.add(lblAddNewPerson);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(170, 33, 116, 22);
		contentPane.add(textField);
		
		JLabel label_1 = new JLabel("Name:");
		label_1.setBounds(209, 13, 56, 16);
		contentPane.add(label_1);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(275, 126, 116, 22);
		contentPane.add(textField_1);
		
		JLabel lblTelephoneNumber = new JLabel("Telephone Number:");
		lblTelephoneNumber.setBounds(275, 106, 116, 16);
		contentPane.add(lblTelephoneNumber);
		
		JButton button = new JButton("Add");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!textField.getText().equals("") && !textField_3.getText().equals(""))
				{
					bank.addPerson(textField.getText(), textField_3.getText());
					setTableContent();
				}
				else
				{
					JOptionPane.showMessageDialog(frame,
						    "Do not leave empty fields!",
						    "Inane warning",
						    JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		button.setBounds(460, 32, 106, 25);
		contentPane.add(button);
		
		JLabel lblEditPerson = new JLabel("Edit Person:");
		lblEditPerson.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblEditPerson.setBounds(12, 126, 90, 16);
		contentPane.add(lblEditPerson);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(130, 126, 116, 22);
		contentPane.add(textField_2);
		
		JLabel label_5 = new JLabel("Name:");
		label_5.setBounds(169, 106, 56, 16);
		contentPane.add(label_5);
		
		JButton button_1 = new JButton("Done");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(selectedPerson != null && !textField_1.getText().equals("") && !textField_2.getText().equals(""))
				{
					bank.editPerson(selectedPerson, textField_2.getText(), textField_1.getText());
					setTableContent();
					clearTextFields();
					JOptionPane.showMessageDialog(frame, "Succes!");
				}
				else
				{
					JOptionPane.showMessageDialog(frame,
						    "Select a person from the table!",
						    "Inane warning",
						    JOptionPane.WARNING_MESSAGE);
				}
				
			}
		});
		button_1.setBounds(418, 125, 90, 25);
		contentPane.add(button_1);
		
		JLabel label_7 = new JLabel("---------------------------------------------------------------------------------------");
		label_7.setFont(new Font("Tahoma", Font.BOLD, 16));
		label_7.setBounds(12, 75, 613, 16);
		contentPane.add(label_7);
		
		JLabel label_8 = new JLabel("---------------------------------------------------------------------------------------");
		label_8.setFont(new Font("Tahoma", Font.BOLD, 16));
		label_8.setBounds(12, 173, 613, 16);
		contentPane.add(label_8);
		
		JLabel lblDeletePerson = new JLabel("Delete Person:");
		lblDeletePerson.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDeletePerson.setBounds(12, 218, 106, 16);
		contentPane.add(lblDeletePerson);
		
		JButton button_2 = new JButton("Delete");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(selectedPerson != null && !textField_5.getText().equals("") && !textField_4.getText().equals(""))
				{
					bank.removePerson(selectedPerson);
					setTableContent();
					clearTextFields();
					JOptionPane.showMessageDialog(frame, "Succes!");
				}
				else
				{
					JOptionPane.showMessageDialog(frame,
						    "Select a person from the table!",
						    "Inane warning",
						    JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		button_2.setBounds(418, 215, 97, 25);
		contentPane.add(button_2);
		
		JLabel label_11 = new JLabel("---------------------------------------------------------------------------------------");
		label_11.setFont(new Font("Tahoma", Font.BOLD, 16));
		label_11.setBounds(12, 263, 613, 16);
		contentPane.add(label_11);
		
		JLabel lblViewAllPersons = new JLabel("View All Persons:");
		lblViewAllPersons.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblViewAllPersons.setBounds(244, 300, 136, 16);
		contentPane.add(lblViewAllPersons);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 346, 624, 266);
		contentPane.add(scrollPane);
		
		table = new JTable();
		
		scrollPane.setViewportView(table);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(318, 33, 116, 22);
		contentPane.add(textField_3);
		
		JLabel label = new JLabel("Telephone Number:");
		label.setBounds(318, 13, 116, 16);
		contentPane.add(label);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(275, 218, 116, 22);
		contentPane.add(textField_4);
		
		JLabel label_2 = new JLabel("Telephone Number:");
		label_2.setBounds(275, 198, 116, 16);
		contentPane.add(label_2);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(130, 218, 116, 22);
		contentPane.add(textField_5);
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				textField_2.setText((String)table.getValueAt(table.getSelectedRow(), 0));
				textField_1.setText((String)table.getValueAt(table.getSelectedRow(), 1));
				textField_5.setText((String)table.getValueAt(table.getSelectedRow(), 0));
				textField_4.setText((String)table.getValueAt(table.getSelectedRow(), 1));
				selectedPerson = new Person(textField_2.getText(), textField_1.getText());
			}
		});
		
		JLabel label_3 = new JLabel("Name:");
		label_3.setBounds(169, 198, 56, 16);
		contentPane.add(label_3);
		
		btnNewButton.setBounds(12, 625, 624, 53);
		contentPane.add(btnNewButton);
		
	}
	
	public void addBackButtonAction(ActionListener a)
    {
		btnNewButton.addActionListener(a);
	}
	
	public void backUpInfo()
	{
		try {
	         FileOutputStream fileOut =
	         new FileOutputStream("bank.ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(bank);
	         out.close();
	         fileOut.close();
	      } catch (IOException i) {
	         i.printStackTrace();
	      }
	}
	
	public void getInfo()
	{
		 try {
	         FileInputStream fileIn = new FileInputStream("bank.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         bank = (Bank) in.readObject();
	         in.close();
	         fileIn.close();
	      } catch (IOException i) {
	         i.printStackTrace();
	         return;
	      } catch (ClassNotFoundException c) {
	         System.out.println("Bank class not found");
	         c.printStackTrace();
	         return;
	      }
	}
	
	public void setTableContent()
	{
		DefaultTableModel model = new DefaultTableModel();
		Person pers = new Person("Blank", "Blank");
		ArrayList<String> buffer = new ArrayList<String>();
		
		
		for(Field f : pers.getClass().getDeclaredFields())
		{
			buffer.add(f.getName());
		}
		buffer.remove(buffer.size()-1);
		model.setColumnIdentifiers((buffer.toArray()));
		
		
		for(Person p : bank.getPersons())
		{
			buffer.clear();
			buffer.add(p.getName());
			buffer.add(p.getTelephoneNumber());
			model.addRow(buffer.toArray());
		}
		table.setModel(model);
	}
	
	private void clearTextFields()
	{
		textField.setText("");
		textField_1.setText("");
		textField_2.setText("");
		textField_3.setText("");
		textField_4.setText("");
		textField_5.setText("");
	}
}
