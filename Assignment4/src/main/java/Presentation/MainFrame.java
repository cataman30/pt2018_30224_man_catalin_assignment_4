package Presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;

public class MainFrame extends JFrame {

	private JPanel contentPane;
	private JLabel lblChooseTheDesired = new JLabel("Choose the desired operation: ");
	private JButton btnManagePersons = new JButton("Manage Persons");
	private JButton btnManageAccounts = new JButton("Manage accounts");
	

	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 562, 438);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblChooseTheDesired.setBounds(175, 37, 282, 16);
		contentPane.add(lblChooseTheDesired);
		
		btnManagePersons.setBounds(68, 98, 403, 67);
		contentPane.add(btnManagePersons);
		
		btnManageAccounts.setBounds(68, 189, 403, 67);
		contentPane.add(btnManageAccounts);
	}
	
	public void btnManagePersonsAction( ActionListener a)
    {
		btnManagePersons.addActionListener(a);
	}
	
	public void btnManageAccountsAction( ActionListener a)
    {
		btnManageAccounts.addActionListener(a);
	}
}
