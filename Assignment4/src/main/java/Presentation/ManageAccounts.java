package Presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Models.Account;
import Models.Bank;
import Models.Person;
import Models.SavingAccount;
import Models.SpendingAccount;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import java.awt.Color;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;

public class ManageAccounts extends JFrame {

	private JPanel contentPane;
	private JTextField textField_2;
	private JTextField fieldInterest;
	private Bank bank = new Bank();
	private ArrayList<Person> holders = new ArrayList<Person>();
	private JOptionPane frame = new JOptionPane();
	private JTable table;
	private Person selected;
	private JComboBox<String> comboBox = new JComboBox<String>();
	private JComboBox<String> comboBox_1 = new JComboBox<String>();
	private JButton button_3 = new JButton("Back");
	private JButton button_1 = new JButton("Done");
	private JLabel valueLabel = new JLabel("Balance:");
	private JLabel lblSelectAnAccount = new JLabel("Select an account from the table!");
	private JLabel lblInterest = new JLabel("Interest:");
	private JLabel lblSelectFromThe = new JLabel("Select from the table!");
	private Account selectedAccount;
	private JLabel lblSelected = new JLabel("Selected!");
	private JButton button_2 = new JButton("Delete");
	private JTextField textField;
	private JTextField textField_1;
	private JLabel lblWithdrawSum = new JLabel("Withdraw sum:");
	private JLabel lblAddSum = new JLabel("Add sum:");
	private JButton btnWithdraw = new JButton("Withdraw");
	private JButton btnAdd = new JButton("Add");
	private float interest;
	private final JLabel lblSelectAnAccount_1 = new JLabel(
			"Select an account from the table to add or withdraw money!");

	public ManageAccounts() {

		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {

				backUpInfo();
				System.exit(0);

			}
		});

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 659, 847);
		contentPane = new JPanel();
		contentPane.setForeground(Color.GREEN);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblAddNewAccount = new JLabel("Add New Account:");
		lblAddNewAccount.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAddNewAccount.setBounds(12, 97, 136, 16);
		contentPane.add(lblAddNewAccount);

		JLabel lblSelectType = new JLabel("Select Type:");
		lblSelectType.setBounds(212, 77, 85, 16);
		contentPane.add(lblSelectType);

		JButton button = new JButton("Add");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (selected != null) {
					int id = bank.getNumberOfAccounts(selected) + 1;
					if (comboBox_1.getSelectedItem().equals("Spending")) {
						bank.addHolderAssociatedAccount(selected, new SpendingAccount(0, id));
						setTableContent();
					} else if (comboBox_1.getSelectedItem().equals("Saving")) {
						bank.addHolderAssociatedAccount(selected, new SavingAccount(0, id));
						setTableContent();
					} else
						JOptionPane.showMessageDialog(frame, "Select a type!", "Inane warning",
								JOptionPane.WARNING_MESSAGE);
					selected = null;
					setHolders();
					comboBox_1.removeAllItems();
				} else {
					JOptionPane.showMessageDialog(frame, "Select a holder!", "Inane warning",
							JOptionPane.WARNING_MESSAGE);
				}

			}
		});
		button.setBounds(356, 94, 106, 25);
		contentPane.add(button);

		JLabel lblEditAccount = new JLabel("Edit Account:");
		lblEditAccount.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblEditAccount.setBounds(12, 173, 106, 16);
		contentPane.add(lblEditAccount);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(130, 173, 116, 22);

		contentPane.add(textField_2);

		textField = new JTextField();
		textField.setBounds(257, 643, 74, 22);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(257, 672, 74, 22);
		contentPane.add(textField_1);

		fieldInterest = new JTextField();
		fieldInterest.setBounds(258, 171, 116, 22);
		contentPane.add(fieldInterest);
		fieldInterest.setColumns(10);

		valueLabel.setBounds(169, 153, 56, 16);
		contentPane.add(valueLabel);
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (selectedAccount instanceof SavingAccount) {
						SavingAccount sa = (SavingAccount) selectedAccount;
						if (textField_2.getText().equals("") && fieldInterest.getText().equals("")) {
							JOptionPane.showMessageDialog(frame, "Invalid Data!", "Inane warning",
									JOptionPane.WARNING_MESSAGE);
							setEditComponentsVisible(false, false);
						} else {
							bank.editHolderAssociatedSavingAccount(selected, selectedAccount,
									Float.parseFloat(textField_2.getText()), Float.parseFloat(fieldInterest.getText()),
									sa.isUsedFlag());
						}
					} else {
						if (textField_2.getText().equals("")) {
							JOptionPane.showMessageDialog(frame, "Invalid Data!", "Inane warning",
									JOptionPane.WARNING_MESSAGE);
							setEditComponentsVisible(false, false);
						} else {
							bank.editHolderAssociatedAccount(selected, selectedAccount,
									Float.parseFloat(textField_2.getText()));
						}
					}
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(frame, "Invalid Data!", "Invalid Data!", JOptionPane.WARNING_MESSAGE);
					setEditComponentsVisible(false, false);
				}

				selected = null;
				selectedAccount = null;
				setTableContent();
				setEditComponentsVisible(false, false);
				textField_2.setText("");
				fieldInterest.setText("");
			}
		});

		button_1.setBounds(425, 170, 90, 25);
		contentPane.add(button_1);

		JLabel label_5 = new JLabel(
				"---------------------------------------------------------------------------------------");
		label_5.setFont(new Font("Tahoma", Font.BOLD, 16));
		label_5.setBounds(12, 132, 613, 16);
		contentPane.add(label_5);

		JLabel label_6 = new JLabel(
				"---------------------------------------------------------------------------------------");
		label_6.setFont(new Font("Tahoma", Font.BOLD, 16));
		label_6.setBounds(12, 210, 613, 16);
		contentPane.add(label_6);

		JLabel lblDeleteAccount = new JLabel("Delete Account:");
		lblDeleteAccount.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDeleteAccount.setBounds(12, 253, 116, 16);
		contentPane.add(lblDeleteAccount);
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bank.removeHolderAssociatedAccount(selected, selectedAccount);
				selected = null;
				selectedAccount = null;
				setEditComponentsVisible(false, false);
				setTableContent();
			}
		});

		button_2.setBounds(297, 250, 97, 25);
		contentPane.add(button_2);

		JLabel label_8 = new JLabel(
				"---------------------------------------------------------------------------------------");
		label_8.setFont(new Font("Tahoma", Font.BOLD, 16));
		label_8.setBounds(12, 288, 613, 16);
		contentPane.add(label_8);

		JLabel label_9 = new JLabel("View All Persons:");
		label_9.setFont(new Font("Tahoma", Font.BOLD, 14));
		label_9.setBounds(244, 317, 136, 16);
		contentPane.add(label_9);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 346, 624, 266);
		contentPane.add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);

		lblSelectFromThe.setBounds(140, 254, 145, 16);
		contentPane.add(lblSelectFromThe);

		button_3.setBounds(12, 734, 624, 53);
		contentPane.add(button_3);

		JLabel label_13 = new JLabel(
				"---------------------------------------------------------------------------------------");
		label_13.setFont(new Font("Tahoma", Font.BOLD, 16));
		label_13.setBounds(12, 54, 613, 16);
		contentPane.add(label_13);

		JLabel lblSelectAHolder = new JLabel("Select a holder:");
		lblSelectAHolder.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblSelectAHolder.setBounds(129, 25, 136, 16);
		contentPane.add(lblSelectAHolder);

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				selected = null;
				for (Person p : holders)
					if (p.getName().equals(table.getValueAt(table.getSelectedRow(), 0)))
						selected = p;
				if (table.getValueAt(table.getSelectedRow(), 5).equals("Saving"))
					selectedAccount = new SavingAccount(
							Float.parseFloat((String) table.getValueAt(table.getSelectedRow(), 2)),
							Integer.parseInt((String) table.getValueAt(table.getSelectedRow(), 1)),
							Boolean.parseBoolean((String) table.getValueAt(table.getSelectedRow(), 4)));
				else
					selectedAccount = new SpendingAccount(
							Float.parseFloat((String) table.getValueAt(table.getSelectedRow(), 2)),
							Integer.parseInt((String) table.getValueAt(table.getSelectedRow(), 1)));

				if (selectedAccount instanceof SavingAccount) {
					interest = Float.parseFloat((String) table.getValueAt(table.getSelectedRow(), 3));
					setEditComponentsVisible(true, true);
				} else
					setEditComponentsVisible(true, false);
			}
		});

		comboBox.setBounds(244, 23, 224, 22);
		contentPane.add(comboBox);

		comboBox_1.setBounds(160, 95, 173, 22);
		contentPane.add(comboBox_1);
		setTableContent();

		lblSelectAnAccount.setForeground(SystemColor.desktop);
		lblSelectAnAccount.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblSelectAnAccount.setBounds(179, 173, 267, 16);
		contentPane.add(lblSelectAnAccount);

		setEditComponentsVisible(false, false);

		JButton btnSelect = new JButton("Select");
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboBox_1.removeAllItems();
				if (!comboBox.getSelectedItem().equals("Not Selected")) {
					comboBox_1.addItem("Not Selected");
					comboBox_1.addItem("Spending");
					comboBox_1.addItem("Saving");
					for (Person p : holders)
						if (p.getName().equals(comboBox.getSelectedItem()))
							selected = p;

				} else {
					JOptionPane.showMessageDialog(frame, "Select a person!", "Inane warning",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		btnSelect.setBounds(479, 22, 97, 25);
		contentPane.add(btnSelect);

		lblInterest.setBounds(288, 153, 56, 16);
		contentPane.add(lblInterest);

		lblSelected.setForeground(Color.GREEN);
		lblSelected.setBounds(169, 254, 56, 16);
		contentPane.add(lblSelected);

		lblWithdrawSum.setBounds(161, 643, 97, 16);
		contentPane.add(lblWithdrawSum);

		lblAddSum.setBounds(161, 672, 97, 16);
		contentPane.add(lblAddSum);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// try {
				if (selectedAccount instanceof SavingAccount) {
					SavingAccount sa = (SavingAccount) selectedAccount;
					if (sa.isUsedFlag() == true) {
						JOptionPane.showMessageDialog(frame, "You cannot add multiple times into a savingAccount!",
								"Inane warning", JOptionPane.WARNING_MESSAGE);
						setEditComponentsVisible(false, false);
					} else {

						bank.addMoney(selected, selectedAccount, Float.parseFloat(textField_1.getText()));
						setTableContent();
						setEditComponentsVisible(false, false);
						selectedAccount = null;
						selected = null;
						textField_1.setText("");
					}
				} else {
					bank.addMoney(selected, selectedAccount, Float.parseFloat(textField_1.getText()));
					setTableContent();
					setEditComponentsVisible(false, false);
					selectedAccount = null;
					selected = null;
					textField_1.setText("");
				}
			}/*
				 * catch(Exception ex) { JOptionPane.showMessageDialog(frame, "Invalid input!",
				 * "Inane warning", JOptionPane.WARNING_MESSAGE); }
				 */
			// }
		});
		btnWithdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (selectedAccount instanceof SavingAccount) {
						SavingAccount sa = (SavingAccount) selectedAccount;
						if (sa.isUsedFlag() == true) {
							JOptionPane.showMessageDialog(frame, "You cannot add multiple times into a savingAccount!",
									"Inane warning", JOptionPane.WARNING_MESSAGE);
							setEditComponentsVisible(false, false);
						} else {
							if (Float.parseFloat(textField.getText()) > sa.getBalance())
								JOptionPane.showMessageDialog(frame, "Insuficient Funds!", "Inane warning",
										JOptionPane.WARNING_MESSAGE);
							else {
								bank.editHolderAssociatedSavingAccount(selected, sa,
										sa.getBalance() - Float.parseFloat(textField.getText()), interest, true);
								setTableContent();
								setEditComponentsVisible(false, false);
								selectedAccount = null;
								selected = null;
								textField.setText("");
							}
						}
					} else {
						if (Float.parseFloat(textField.getText()) > selectedAccount.getBalance())
							JOptionPane.showMessageDialog(frame, "Insuficient Funds!", "Inane warning",
									JOptionPane.WARNING_MESSAGE);
						else {
							bank.editHolderAssociatedAccount(selected, selectedAccount,
									selectedAccount.getBalance() - Float.parseFloat(textField.getText()));
							setTableContent();
							setEditComponentsVisible(false, false);
							textField.setText("");
							selectedAccount = null;
							selected = null;
						}
					}
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(frame, "Invalid input!", "Inane warning",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		});

		btnWithdraw.setBounds(349, 643, 97, 25);
		contentPane.add(btnWithdraw);

		btnAdd.setBounds(349, 671, 97, 25);
		contentPane.add(btnAdd);
		lblSelectAnAccount_1.setForeground(Color.RED);
		lblSelectAnAccount_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSelectAnAccount_1.setBounds(94, 658, 487, 16);

		contentPane.add(lblSelectAnAccount_1);

	}

	public void backUpInfo() {
		try {
			FileOutputStream fileOut = new FileOutputStream("bank.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(bank);
			out.close();
			fileOut.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
	}

	public void getInfo() {
		try {
			FileInputStream fileIn = new FileInputStream("bank.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			bank = (Bank) in.readObject();

			for (Person p : bank.getPersons()) {
				for (Account acc : bank.getAccounts(p)) {
					acc.addObserver(p);
				}
			}

			in.close();
			fileIn.close();
		} catch (IOException i) {
			i.printStackTrace();
			return;
		} catch (ClassNotFoundException c) {
			System.out.println("Bank class not found");
			c.printStackTrace();
			return;
		}
	}

	public void setTableContent() {
		DefaultTableModel model = new DefaultTableModel();
		Class acc = Account.class;
		ArrayList<String> buffer = new ArrayList<String>();
		SavingAccount sa;

		buffer.add("Holder");
		for (Field f : acc.getDeclaredFields()) {
			buffer.add(f.getName());
		}

		buffer.remove(buffer.size() - 1);
		buffer.add("Interest");
		buffer.add("Used");
		buffer.add("Type");
		model.setColumnIdentifiers((buffer.toArray()));
		buffer.clear();

		for (Person p : bank.getPersons()) {
			buffer.clear();
			for (Account a : bank.getAccounts(p)) {
				buffer.clear();
				buffer.add(p.getName());
				buffer.add(Integer.toString(a.getId()));
				buffer.add(Float.toString(a.getBalance()));
				if (a instanceof SavingAccount) {
					sa = (SavingAccount) a;
					buffer.add(Float.toString(sa.getInterest()));
					buffer.add(Boolean.toString(sa.isUsedFlag()));
					buffer.add("Saving");
				} else {
					buffer.add("-");
					buffer.add("-");
					buffer.add("Spending");
				}
				if (!buffer.isEmpty())
					model.addRow(buffer.toArray());
			}
		}
		table.setModel(model);
	}

	public void setHolders() {
		comboBox.removeAllItems();
		comboBox.addItem("Not Selected");
		holders.clear();

		for (Person p : bank.getPersons()) {
			comboBox.addItem(p.getName());
			holders.add(p);
		}
	}

	public void setEditComponentsVisible(boolean choice, boolean saving) {
		textField_2.setVisible(choice);
		button_1.setVisible(choice);
		valueLabel.setVisible(choice);
		lblSelectFromThe.setVisible(!choice);
		lblSelectAnAccount.setVisible(!choice);
		button_2.setVisible(choice);
		textField.setVisible(choice);
		textField_1.setVisible(choice);
		btnAdd.setVisible(choice);
		btnWithdraw.setVisible(choice);
		lblWithdrawSum.setVisible(choice);
		lblAddSum.setVisible(choice);
		lblSelected.setVisible(choice);
		lblInterest.setVisible(saving);
		fieldInterest.setVisible(saving);
		lblSelectAnAccount_1.setVisible(!choice);
	}

	public void addBackButtonAction(ActionListener a) {
		button_3.addActionListener(a);
	}
}
