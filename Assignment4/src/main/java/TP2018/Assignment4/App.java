package TP2018.Assignment4;

import Models.*;
import Presentation.MainFrame;
import Presentation.ManageAccounts;
import Presentation.ManagePersons;

public class App 
{
    public static void main( String[] args )
    {
    	ManagePersons mp = new ManagePersons();
    	ManageAccounts ma = new ManageAccounts();
    	MainFrame mf = new MainFrame();
    	mf.setVisible(true);
    	
    	mf.btnManagePersonsAction(a->{
    		mp.getInfo();
    		mp.setTableContent();
    		mp.setVisible(true);
    		mf.setVisible(false);
    	});
    	
    	mf.btnManageAccountsAction(a->{
    		ma.getInfo();
    		ma.setHolders();
    		ma.setTableContent();
    		ma.setVisible(true);
    		mf.setVisible(false);
    	});
    	
    	mp.addBackButtonAction(a->{
    		mp.backUpInfo();
    		mp.setVisible(false);
    		mf.setVisible(true);
    	});
    	
    	ma.addBackButtonAction(a->{
    		ma.backUpInfo();
    		ma.setVisible(false);
    		mf.setVisible(true);
    	});
    }
}
