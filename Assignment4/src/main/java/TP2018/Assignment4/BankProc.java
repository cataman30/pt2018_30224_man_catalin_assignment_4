package TP2018.Assignment4;

import Models.Account;
import Models.Person;

public interface BankProc {

	/*
	 * pre: arguments are not null and bank is well formed
	 * post: bank size is greater than before adding and bank is well formed;
	 */
	public void addPerson(String name, String telephoneNumber);
	/*
	 * pre: arguments are not null and bank is well formed
	 * post: bank size is lesser than before removing and bank is well formed;
	 */
	public void removePerson(Person toBeRemoved);
	/*
	 * pre: arguments are not null and bank is well formed
	 * post: bank is well formed;
	 */
	public void editPerson(Person toBeRemoved ,String newName, String newTelephoneNumber);
	/*
	 * pre: arguments are not null and bank is well formed
	 * post: the size of the accounts has increased and bank is well formed;
	 */
	public void addHolderAssociatedAccount(Person holder, Account newAccount);
	/*
	 * pre: arguments are not null and bank is well formed
	 * post: the size of the accounts has decreased and bank is well formed;
	 */
	public void removeHolderAssociatedAccount(Person holder, Account toBeRemoved);
	/*
	 * pre: arguments are not null and bank is well formed
	 * post: bank is well formed;
	 */
	public void editHolderAssociatedAccount(Person holder, Account account, float newBalance);
}
