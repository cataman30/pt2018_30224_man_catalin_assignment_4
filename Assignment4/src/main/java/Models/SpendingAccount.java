package Models;

public class SpendingAccount extends Account{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SpendingAccount(float balance, int id) 
	{
		super(balance, id);
	}

	@Override
	public void addMoney(float m)
	{
		this.setBalance(this.getBalance() + m);
		setChanged();
		notifyObservers(m);
	}

	@Override
	public void withdrawMoney(float m) {
		this.setBalance(this.getBalance() - m);
		setChanged();
		notifyObservers(m);
		
	}
}
