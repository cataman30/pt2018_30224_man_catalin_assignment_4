package Models;

public class SavingAccount extends Account{

	private boolean usedFlag = false;
	private float interest = 0;
	private static final long serialVersionUID = 1L;
	
	public SavingAccount(float balance, int id, boolean used) 
	{
		super(balance, id);
		usedFlag = used;
	}
	
	public SavingAccount(float balance, int id) 
	{
		super(balance, id);
	}
	
	public boolean isUsedFlag() {
		return usedFlag;
	}

	public void setUsedFlag(boolean usedFlag) {
		this.usedFlag = usedFlag;
	}

	public float getInterest() {
		return interest;
	}
	public void setInterest(float interest) {
		this.interest = interest;
	}
	
	@Override
	public void addMoney(float m)
	{
		this.setBalance(this.getBalance() + m-m/100*interest);
		usedFlag = true;
		setChanged();
		notifyObservers(m);
	}
	
	@Override
	public void withdrawMoney(float m)
	{
		this.setBalance(this.getBalance() - m);
		setChanged();
		notifyObservers(m);
	}

}
