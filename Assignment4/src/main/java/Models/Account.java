package Models;

import java.io.Serializable;
import java.util.Observable;

public abstract class Account extends Observable implements Serializable{

	private int id;
	private float balance;
	private static final long serialVersionUID = 1L;
	
	public Account(float balance, int id) {
		super();
		this.balance = balance;
		this.id = id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public abstract void addMoney(float m);
	
	public abstract void withdrawMoney(float m);
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	public float getBalance() 
	{
		return balance;
	}

	public void setBalance(float balance) 
	{
		this.balance = balance;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
