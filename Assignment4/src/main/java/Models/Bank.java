package Models;


import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

import TP2018.Assignment4.BankProc;

public class Bank implements BankProc, Serializable{

	private static final long serialVersionUID = 1L;
	private Map<Person, HashSet<Account>> bank = new HashMap<Person, HashSet<Account>>();
	
	public boolean isWellFormed()
	{
		for(Person p : bank.keySet())
		{
			for(Account a : this.getAccounts(p))
			{
				if(a.getBalance()<0)
					return false;
			}
		}
		
		return true;
	}

	public void addPerson(String name, String telephoneNumber) {
	
		try {
		assert name != null;
		assert isWellFormed();
		
		int size = bank.size();
		Person p = new Person(name, telephoneNumber);
		
		
		if(!bank.containsKey(p))
			bank.put(p, new HashSet<Account>());
		
		assert bank.size() > size;
		assert isWellFormed();}catch(AssertionError ae) {
			System.out.println("Failed to add a person!");
		}
		
	}

	public void removePerson(Person toBeRemoved) 
	{
		try {
		assert toBeRemoved != null;
		assert isWellFormed();
		
		int size = bank.size();
		
				if(bank.containsKey(toBeRemoved))
				bank.remove(toBeRemoved);

		assert size > bank.size();
		assert isWellFormed();}catch(AssertionError ae) {
			System.out.println("Failed to remove a person!");
		}
		
	}
	
	public void editPerson(Person toBeRemoved ,String newName, String newTelephoneNumber)
	{
		
		try {
		assert toBeRemoved!=null && newName != null && newTelephoneNumber != null;
		assert isWellFormed();
		for(Person p : bank.keySet())
		{
			if(p.getName().equals(toBeRemoved.getName()) && p.getTelephoneNumber().equals(toBeRemoved.getTelephoneNumber()))
				{
					p.setName(newName);
					p.setTelephoneNumber(newTelephoneNumber);
					break;
				}
		}
		assert isWellFormed();}catch(AssertionError ae) {
			System.out.println("Failed to edit a person!");
		}
	}

	public void addHolderAssociatedAccount(Person holder, Account newAccount) 
	{
		try {
			assert holder != null && newAccount!=null;
			assert isWellFormed();
			int size = this.getAccounts(holder).size();
			
			bank.get(holder).add(newAccount);
			
		assert this.getAccounts(holder).size()>size;
		assert isWellFormed();
		}catch(AssertionError ae) {
			System.out.println("Failed to add an account!");
		}
		
	}

	public void removeHolderAssociatedAccount(Person holder, Account toBeRemoved) 
	{
		try {
			assert holder != null && toBeRemoved!=null;
			assert isWellFormed();
			int size = this.getAccounts(holder).size();
		
			bank.get(holder).remove(toBeRemoved);
		
		assert this.getAccounts(holder).size()<size;
		assert isWellFormed();
	}catch(AssertionError ae) {
		System.out.println("Failed to remove an account!");
	}
	}
	
	public void editHolderAssociatedAccount(Person holder, Account account, float newBalance)
	{
		try {
			assert holder!=null && account!=null;
			assert isWellFormed();
		for(Person p : bank.keySet())
		{
			
			if(p.getName().equals(holder.getName()) && p.getTelephoneNumber().equals(holder.getTelephoneNumber()))
				for(Account a : bank.get(p))
				{
					if(account.getId() == a.getId())
					{
						a.setBalance(newBalance);
						break;
					}
				}
		}
		assert isWellFormed();
		}catch(AssertionError ae) {
			System.out.println("Failed to edit an account!");
		}
	}
	
	public void editHolderAssociatedSavingAccount(Person holder, Account account, float newBalance, float interest, boolean used)
	{
		try {
			assert holder!=null && account!=null && interest>0;
			assert isWellFormed();
		for(Person p : bank.keySet())
		{
			if(p.getName().equals(holder.getName()) && p.getTelephoneNumber().equals(holder.getTelephoneNumber()))
				{for(Account a : bank.get(p))
				{
					if(account.getId() == a.getId())
					{
						SavingAccount sa = (SavingAccount) a;
						sa.setInterest(interest);
						sa.setBalance(newBalance);
						sa.setUsedFlag(used);
						System.out.println(used);
						a=sa;
						break;
					}
				}
				break;}
		}
		assert isWellFormed();
		}catch(AssertionError ae) {
			System.out.println("Failed to edit a saving account!");
		}
	}
	
	public Set<Person> getPersons()
	{
		return bank.keySet();
	}
	
	public HashSet<Account> getAccounts(Person p)
	{
		
		HashSet<Account> accounts = bank.get(p);
		
		return accounts;
	}
	
	public Map<Person, HashSet<Account>> getBank()
	{
		return bank;
	}
	
	public int getNumberOfAccounts(Person p)
	{
		return bank.get(p).size();
	}
	
	public void addMoney(Person holder, Account ac, float money)
	{
		for(Account a : getAccounts(holder))
			if(a.getId() == ac.getId())
				a.addMoney(money);
	}
	
	public void withdrawMoney(Person holder, Account ac, float money)
	{
		for(Account a : getAccounts(holder))
			if(a.getId() == ac.getId())
				a.withdrawMoney(money);
	}

}
